// -----------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -----------------------------------------------------------------------------

#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv)
{
  (void)argc;
  (void)argv;
  // Initialize the MPI environment
  MPI_Init(NULL, NULL);

  // Get the number of processes
  int world_size = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Get the rank of the process
  int world_rank = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Get the name of the processor
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len = 0;
  MPI_Get_processor_name(processor_name, &name_len);

  // Print off a hello world message
  printf(
    "Hello world from processor %s, rank %d out of %d processors\n",
    processor_name,
    world_rank,
    world_size);

  // Finalize the MPI environment.
  MPI_Finalize();
}
