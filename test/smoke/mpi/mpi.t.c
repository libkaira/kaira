// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
  MPI_Init(NULL, NULL);
  // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  int result = 0;
  MPI_Allreduce(&world_rank, &result, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  int expected_result = world_size * (world_size - 1) / 2;
  printf("Expected Result = %d\n", expected_result);
  printf("Result = %d\n", result);

  if (result != expected_result) {
    printf("ERROR! Result does not match expected result.\n");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
