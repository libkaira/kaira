# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

add_subdirectory(cpp)

if(PROJECT_FEATURE_MPI)
  add_subdirectory(mpi)
endif()
