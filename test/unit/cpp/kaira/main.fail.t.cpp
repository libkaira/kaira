// -----------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2021-2021, Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -----------------------------------------------------------------------------

#include <cstdlib>

auto alpha() -> int;

auto alpha() -> int
{
  return 0;
}

auto main() -> int
{
  return EXIT_FAILURE;
}
