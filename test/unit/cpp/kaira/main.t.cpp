// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <catch2/catch_test_macros.hpp>
#include <testkaira/main.hpp>

/* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/
KAIRA_CATCH_MAIN("test/unit/kaira/cpp/main")

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
TEST_CASE("Main: Dummy", "[all]")
{
  REQUIRE(1 == 1);
}
