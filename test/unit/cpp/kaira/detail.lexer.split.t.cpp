// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <kaira/detail/lexer/split.hpp>
#include <testmol/compat/catch_main.hpp>

/* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/
TESTMOL_CATCH_MAIN("test/unit/cpp/detail/lexer/split")

TEST_CASE("01: split", "[all]")
{
  auto const input = std::string("a,b,c");
  auto const result = kaira::detail::lexer::split(input, ',');
  REQUIRE(result.size() == 3);
  REQUIRE(result[0] == "a");
  REQUIRE(result[1] == "b");
  REQUIRE(result[2] == "c");
}

TEST_CASE("02: split_commandline", "[all]")
{
  auto const input = std::vector<std::string>{"a,b,c", "d,e,f"};
  auto const result = kaira::detail::lexer::split_commandline(input);
  REQUIRE(result.size() == 6);
  REQUIRE(result[0] == "a");
  REQUIRE(result[1] == "b");
  REQUIRE(result[2] == "c");
  REQUIRE(result[3] == "d");
  REQUIRE(result[4] == "e");
  REQUIRE(result[5] == "f");
}
