// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#ifndef TESTKAIRA_DATA_HPP
#define TESTKAIRA_DATA_HPP

#include <string>

namespace testkaira::data {
class path {
public:
  path() = default;
  path(std::string input, std::string output);

public:
  auto input() const noexcept -> std::string const&;
  auto output() const noexcept -> std::string const&;

private:
  std::string input_;
  std::string output_;
};

auto path_from_prefix(path const& prefix, std::string const& suffix) -> path;
} // namespace testkaira::data

#endif // TESTKAIRA_DATA_HPP
