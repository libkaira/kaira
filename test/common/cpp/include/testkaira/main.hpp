// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#ifndef TESTKAIRA_MAIN_HPP
#define TESTKAIRA_MAIN_HPP

#include <testkaira/data.hpp>
#include <testkaira/detail/runner.hpp>

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define KAIRA_CATCH_MAIN(suffix)                                                                 \
                                                                                                   \
  /* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/                          \
  extern testkaira::data::path project_prefix;                                                   \
  /* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/                          \
  extern testkaira::data::path group_prefix;                                                     \
                                                                                                   \
  /* path_prefix : Paths Corresponding to the Complete Project */                                  \
  testkaira::data::path project_prefix = testkaira::data::path();                              \
  /* path_prefix : Paths Corresponding to The Current Group */                                     \
  testkaira::data::path group_prefix = testkaira::data::path();                                \
                                                                                                   \
  auto main(int argc, char* argv[])->int                                                           \
  {                                                                                                \
    return testkaira::detail::runner(                                                            \
      argc, argv, &project_prefix, &group_prefix, std::string(suffix));                            \
  }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define KAIRA_CATCH_GROUP()                                                                      \
                                                                                                   \
  /* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/                          \
  extern testkaira::data::path project_prefix;                                                   \
  /* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/                          \
  extern testkaira::data::path group_prefix;

#endif // TESTKAIRA_MAIN_HPP
