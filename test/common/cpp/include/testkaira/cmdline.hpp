// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#ifndef TESTKAIRA_CMDLINE_HPP
#define TESTKAIRA_CMDLINE_HPP

#include <memory>
#include <string>
#include <testkaira/data.hpp>
#include <vector>

namespace testkaira::cmdline {
class result {
public:
  result() = default;
  result(testkaira::data::path path, std::vector<std::string> cmdline)
  : path_(std::move(path)), cmdline_(std::move(cmdline))
  {
  }

public:
  auto path() const noexcept -> testkaira::data::path const& { return path_; }
  auto residual() const noexcept -> std::vector<std::string> const& { return cmdline_; }

private:
  testkaira::data::path path_;
  std::vector<std::string> cmdline_;
};

auto parse(int argc, char** argv) -> result;
// NOLINTNEXTLINE(modernize-avoid-c-arrays)
auto to_native_view(std::vector<std::string> const& cmdline) -> std::unique_ptr<char*[]>;
} // namespace testkaira::cmdline

#endif // TESTKAIRA_CMDLINE_HPP
