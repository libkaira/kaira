// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#ifndef TESTKAIRA_DETAIL_RUNNER_HPP
#define TESTKAIRA_DETAIL_RUNNER_HPP

#include <testkaira/data.hpp>

namespace testkaira::detail {
auto runner(
  int argc,
  char** argv,
  testkaira::data::path* project_prefix,
  testkaira::data::path* group_prefix,
  std::string const& suffix) -> int;
} // namespace testkaira::detail

#endif // TESTKAIRA_DETAIL_RUNNER_HPP
