// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#ifndef TESTMOL_COMPAT_CMDLINE_HPP
#define TESTMOL_COMPAT_CMDLINE_HPP

#include <memory>
#include <string>
#include <testmol/compat/data.hpp>
#include <vector>

namespace testmol::compat::cmdline {
class result {
public:
  result() = default;
  result(testmol::compat::data::path path, std::vector<std::string> cmdline)
  : path_(std::move(path)), cmdline_(std::move(cmdline))
  {
  }

public:
  auto path() const noexcept -> testmol::compat::data::path const& { return path_; }
  auto residual() const noexcept -> std::vector<std::string> const& { return cmdline_; }

private:
  testmol::compat::data::path path_;
  std::vector<std::string> cmdline_;
};

auto parse(int argc, char** argv) -> result;
// NOLINTNEXTLINE(modernize-avoid-c-arrays)
auto to_native_view(std::vector<std::string> const& cmdline) -> std::unique_ptr<char*[]>;
} // namespace testmol::compat::cmdline

#endif // TESTMOL_COMPAT_CMDLINE_HPP
