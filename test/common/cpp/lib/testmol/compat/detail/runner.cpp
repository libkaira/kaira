// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <catch2/catch_session.hpp>
#include <testmol/compat/cmdline.hpp>
#include <testmol/compat/data.hpp>
#include <testmol/compat/detail/runner.hpp>

namespace testmol::compat::detail {
auto runner(
  int argc,
  char** argv,
  testmol::compat::data::path* project_prefix,
  testmol::compat::data::path* group_prefix,
  std::string const& suffix) -> int
{
  auto result = testmol::compat::cmdline::parse(argc, argv);

  *project_prefix = testmol::compat::data::path(result.path());
  *group_prefix = testmol::compat::data::path_from_prefix(*project_prefix, suffix);

  int const residual_argc = static_cast<int>(result.residual().size());
  auto const residual_argv = testmol::compat::cmdline::to_native_view(result.residual());

  Catch::Session session;

  auto cli = session.cli();

  session.cli(cli);

  int const return_code = session.applyCommandLine(residual_argc, residual_argv.get());
  if (return_code != 0) {
    return return_code;
  }

  return session.run();
}

} // namespace testmol::compat::detail
