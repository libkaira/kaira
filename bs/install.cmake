# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

if(BUILD_SOURCE)
  add_library(kaira::libkaira ALIAS libkaira)
  install(TARGETS libkaira EXPORT kairaTarget FILE_SET HEADERS)

  install(FILES ${PROJECT_BINARY_DIR}/lib/kairaConfigVersion.cmake
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/kaira/cmake/)

  install(FILES ${PROJECT_BINARY_DIR}/lib/kairaConfig.cmake
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/kaira/cmake)

  install(
    EXPORT kairaTarget
    FILE kairaTarget.cmake
    NAMESPACE kaira::
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/kaira/cmake)
endif()


if(BUILD_DOC)
  install(DIRECTORY ${PROJECT_BINARY_DIR}/documentation
    DESTINATION ${CMAKE_INSTALL_DOCDIR})
endif()


