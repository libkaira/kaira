# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------
#  Print All Options and Version for The User
# --------------------------------------------------------------------------------------------------
message(STATUS "Build: Documentation          -     ${BUILD_DOC}")
message(STATUS "Build: Source                 -     ${BUILD_SOURCE}")
message(STATUS "Build: Testing                -     ${BUILD_TESTING}")
message(STATUS "Build: Benchmarking           -     ${BUILD_BENCHMARKING}")

message(STATUS "Diagnostic: Coverage          -     ${DIAGNOSTIC_COVERAGE}")
message(STATUS "Diagnostic: Format            -     ${DIAGNOSTIC_FORMAT}")
message(STATUS "Diagnostic: Linting           -     ${DIAGNOSTIC_LINTING}")

message(STATUS "Diagnostic: Address Sanitizer -     ${DIAGNOSTIC_ASAN}")
message(STATUS "Diagnostic: Leak Sanitizer    -     ${DIAGNOSTIC_LSAN}")
message(STATUS "Diagnostic: UB Sanitizer      -     ${DIAGNOSTIC_UBSAN}")

message(STATUS "Warning: Deprecated           -     ${WARNING_DEPRECATED}")
message(STATUS "Warning: Mode                 -     ${WARNING_MODE}")
message(STATUS "Warning: Werror               -     ${WARNING_AS_ERROR}")

message(STATUS "Features:                     -     ${PROJECT_FEATURE}")

message(STATUS "Package Builder ID            -     ${PKG_BUILDER_ID}")
message(STATUS "Package Builder Revision      -     ${PKG_BUILDER_REVISION}")

message(STATUS "Git Branch                    -     ${GIT_BRANCH}")
message(STATUS "Release Type                  -     ${RELEASE_TYPE}")
message(STATUS "Release Revision              -     ${RELEASE_REVISION}")

message(STATUS "Semantic Version              -     ${PROJECT_SEMVER}")
message(STATUS "Build Version                 -     ${PROJECT_BUILD_VERSION}")


