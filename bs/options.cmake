# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2021-2021, Jayesh Badwaik <jayesh@badwaik.dev>
# --------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------
#  User Options
# --------------------------------------------------------------------------------------------------
# BUILD_DOC (Option)
# If enabled, the build will also build the documentation to go along with the
# software. The build documentation is stored under the `doc` sub-directory in
# the build directory, and the documentation will be installed in
# `<prefix>/share/kaira/doc`.
option(BUILD_DOC "Build Documentation" Off)

# --------------------------------------------------------------------------------------------------
#  Warnings
# --------------------------------------------------------------------------------------------------
# WARNINGS_AS_ERROR (Option)
# If enabled, the build will treat all warnings as errors.
option(WARNING_AS_ERROR "Treat Warnings as Errors" Off)

# WARNING_DEPRECATED (Option)
# If enabled, the build will treat all uses of deprecated features as warnings.
option(WARNING_DEPRECATED "Warnings For Use of Deprecated Features" Off)

# WARNING_MODE (Variable)
# The variable selects the warning profile to be enabled for building the software.
# Kaira supports the following warning modes:
# a. `none`         : Enable No Warnings
# b. `essential`    : Enable Essential Warnings
# c. `mandatory`    : Enable Mandatory Warnings
# d. `recommended`  : Enable Recommended Warnings
# e. `everything`   : Enable "Weverything"
set(WARNING_MODE "recommended" CACHE STRING "Warning Mode")

# --------------------------------------------------------------------------------------------------
#  Dev Features
# --------------------------------------------------------------------------------------------------
# BUILD_SOURCE (Option)
# If disabled, the build will try to use the vendored version of the software
# instead of using the provided source version. This option is useful to verify
# the correctness of a vendored/installed version of the software.
option(BUILD_SOURCE "Build Source" On)

# BUILD_TESTING (Option)
# If enabled, the build will build the tests. The selection of the tests built
# is given by the variable `BUILD_TESTING_MODE`.
option(BUILD_TESTING "Build Test Suite" Off)

# DIAGNOSTIC_FORMAT (Option)
# If enabled, the build will enable a target named `diagnostic.format` which
# will check if the code is formatted correctly.
option(DIAGNOSTIC_FORMAT "Diagnostic: Format" Off)

# DIAGNOSTIC_LINTING (Option)
# If enabled, the build will enable a target named `diagnostic.linting` which
# will run all the linters through the code.
option(DIAGNOSTIC_LINTING "Diagnostic: Linting" Off)

# DIAGNOSTIC_COVERAGE (Option)
# If enabled, the build will enable a target named `diagnostic.coverage` which
# will run diagnostic for the coverage of the tests.
option(DIAGNOSTIC_COVERAGE "Diagnostic: Coverage" Off)

# DIAGNOSTIC_ASAN (Option)
# If enabled, the build will enable diagnostics with address sanitizer.
option(DIAGNOSTIC_ASAN "Diagnostic: Address Sanitizer" Off)

# DIAGNOSTIC_LSAN (Option)
# If enabled, the build will enable diagnostics with leak sanitizer.
option(DIAGNOSTIC_LSAN "Diagnostic: Leak Sanitizer" Off)

# DIAGNOSTIC_UBSAN (Option)
# If enabled, the build will enable diagnostics with UB sanitizer.
option(DIAGNOSTIC_UBSAN "Diagnostic: Undefined Sanitizer" Off)

# --------------------------------------------------------------------------------------------------
#  Options for Packagers
# --------------------------------------------------------------------------------------------------
# PKG_BUILDER_ID (Variable)
# The variable sets an ID for the builder of the software. The variable is
# meant to be used by the package maintainers who want to assign a builder
# identification which points back to themselves.
set(PKG_BUILDER_ID "unknown" CACHE STRING "ID for a Package Maintainer")

# PKG_BUILDER_REVISION (Variable)
# The variable sets a revision for the builder of the software. The variable is
# meant to be used by the package maintainers who want to assign a build
# revision on top of the software version.
set(PKG_BUILDER_REVISION "1" CACHE STRING "Revision of Release")

# --------------------------------------------------------------------------------------------------
#  Options for Versioninig
# --------------------------------------------------------------------------------------------------
# RELEASE_TYPE (Variable)
# The variable selects the type of release or prerelease version to be used.
# The following release types are supported
# a. `dev` : The version is not a release. Use the git-based version instead.
# b. `release` : The version is a release.
# c. `alpha` : The version is an alpha release.
# d. `beta` : The version is a beta release
# e. `rc` : The version is an RC Release
set(RELEASE_TYPE "dev" CACHE STRING "Type of Release")

# RELEASE_VERSION (Variable)
# The variable selects the revision of release under consideration. For
# example, for a `rc-1` pre-release. The RELEASE_REVISION would be `1`.
set(RELEASE_REVISION "1" CACHE STRING "Revision of Release")

# --------------------------------------------------------------------------------------------------
# Option for Build Script Developers
# ATTN: ONLY USE THIS WHEN EXPERIMENTING WITH CMAKE BUILD SCRIPTS
# --------------------------------------------------------------------------------------------------
option(PROJECT_DEV_ENABLE_TORABUILD "Enable Torabuild" On)
mark_as_advanced(PROJECT_DEV_ENABLE_TORABUILD)
