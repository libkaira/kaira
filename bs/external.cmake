# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
if(BUILD_TESTING)
  find_package(Catch2 REQUIRED)
endif()

if(NOT BUILD_SOURCE)
  find_package(kaira REQUIRED)
endif()

find_package(Threads REQUIRED)

if(PROJECT_FEATURE_MPI)
  find_package(tora REQUIRED COMPONENTS mpi)
endif()

if(DIAGNOSTIC_ASAN)
  find_package(tora REQUIRED COMPONENTS instrument.asan)
endif()

if(DIAGNOSTIC_LSAN)
  add_compile_definitions(DIAGNOSTIC_LSAN)
  find_package(tora REQUIRED COMPONENTS instrument.lsan)
endif()

if(DIAGNOSTIC_UBSAN)
  add_compile_definitions(DIAGNOSTIC_UBSAN)
  find_package(tora REQUIRED COMPONENTS instrument.ubsan)
endif()

find_package(jnum REQUIRED)
