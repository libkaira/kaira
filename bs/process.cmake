# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------
# Compute Version
# --------------------------------------------------------------------------------------------------

tora_version_tree_hash(GIT_TREE_HASH)
tora_version_branch(GIT_BRANCH)

set(GIT_VERSION "${GIT_BRANCH}.${GIT_TREE_HASH}")
set(PKG_BUILDER_INFO "${PKG_BUILDER_ID}-${PKG_BUILDER_REVISION}")
set(RELEASE_INFO  "${RELEASE_TYPE}.${RELEASE_REVISION}")

if(${RELEASE_TYPE} STREQUAL "release")
  set(PROJECT_SEMVER            "${PROJECT_VERSION}")
  set(PROJECT_BUILD_VERSION     "${PROJECT_VERSION}-${PKG_BUILDER_INFO}")

elseif(${RELEASE_TYPE} STREQUAL "dev")
  set(PROJECT_SEMVER            "${PROJECT_VERSION}-dev.${GIT_VERSION}")
  set(PROJECT_BUILD_VERSION     "${PROJECT_VERSION}-dev.${GIT_VERSION}-${PKG_BUILDER_INFO}")

else()
  set(PROJECT_SEMVER            "${PROJECT_VERSION}-${RELEASE_INFO}")
  set(PROJECT_BUILD_VERSION     "${PROJECT_VERSION}-${RELEASE_INFO}-${PKG_BUILDER_INFO}")

endif()


if(BUILD_DOC)
  add_custom_target(documentation)
  set_target_properties(documentation PROPERTIES EXCLUDE_FROM_ALL False)

  find_package(tora REQUIRED COMPONENTS doxygen)

  tora_configure_doxygen(
    documentation
    ${PROJECT_BINARY_DIR}/documentation/doxygen
    ${PROJECT_SOURCE_DIR}/doc/doxygen/doxyfile.in)
endif()
