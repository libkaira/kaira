// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#ifndef KAIRA_DETAIL_LEXER_SPLIT_HPP
#define KAIRA_DETAIL_LEXER_SPLIT_HPP

#include <string>
#include <vector>

namespace kaira::detail::lexer {
auto split_commandline(std::vector<std::string> const& cmdline) -> std::vector<std::string>;
auto split(std::string const& input, char delim) -> std::vector<std::string>;
} // namespace kaira::detail::lexer

#endif // KAIRA_DETAIL_LEXER_SPLIT_HPP
