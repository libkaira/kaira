// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <kaira/detail/lexer/split.hpp>

#include <iostream>

namespace kaira::detail::lexer {

auto split_commandline(std::vector<std::string> const& cmdline) -> std::vector<std::string>
{
  auto result = std::vector<std::string>{};
  for (auto const& str : cmdline) {
    auto const split_str = split(str, ',');
    result.insert(result.end(), split_str.begin(), split_str.end());
  }
  return result;
}

auto split(std::string const& input, char delim) -> std::vector<std::string>
{
  auto result = std::vector<std::string>{};
  std::size_t last = 0;
  std::size_t next = 0;

  bool continue_loop = true;
  while (continue_loop) {
    next = input.find(delim, last);
    if (next == std::string::npos) {
      continue_loop = false;
    }
    result.push_back(input.substr(last, next - last));
    last = next + 1;
  }

  return result;
}
} // namespace kaira::detail::lexer
