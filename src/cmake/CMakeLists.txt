# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

include(CMakePackageConfigHelpers)

configure_package_config_file(
  ${CMAKE_CURRENT_SOURCE_DIR}/kairaConfig.cmake.in
  ${PROJECT_BINARY_DIR}/lib/kairaConfig.cmake
  INSTALL_DESTINATION ${CMAKE_INSTALL_LIBDIR})

write_basic_package_version_file(
  ${PROJECT_BINARY_DIR}/lib/kairaConfigVersion.cmake
  VERSION ${CMAKE_PROJECT_VERSION}
  COMPATIBILITY SameMajorVersion)


