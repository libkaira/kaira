#!/bin/bash
# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

set -euo pipefail


EXPECTED_VERSION="v1.1.0"
EXPECTED_VERSION_TYPE="tags"
EXPECTED_MIRROR="https://github.com/TartanLlama/expected"
EXPECTED_URL="$EXPECTED_MIRROR/archive/refs/$EXPECTED_VERSION_TYPE/$EXPECTED_VERSION.tar.gz"
EXPECTED_SHA256SUM="1db357f46dd2b24447156aaf970c4c40a793ef12a8a9c2ad9e096d9801368df6"

SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


source $SCRIPT_DIRECTORY/common.sh

process_cmdline_argument $@

export PATH=$PKGROOT/usr/bin:$PATH

TMP_DIR=$PKGROOT/var/tmp/expected/

mkdir -p $TMP_DIR
mkdir -p $TMP_DIR/download

wget $EXPECTED_URL -O $TMP_DIR/download/expected.tar.gz

pushd $TMP_DIR/download
if [[ $EXPECTED_SHA256SUM != "SKIP" ]]; then
  echo "$EXPECTED_SHA256SUM expected.tar.gz" | sha256sum --check
fi
popd


pushd $TMP_DIR
mkdir -p src
pushd src
mkdir -p expected
tar xf ../download/expected.tar.gz -C expected --strip-components 1
popd

mkdir -p build
pushd build
cmake --toolchain $TOOLCHAIN --install-prefix $PKGROOT/usr ../src/expected -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=Off

cmake --build .
cmake --install .
popd
popd

