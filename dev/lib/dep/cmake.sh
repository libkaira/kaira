#!/bin/bash
# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
set -euo pipefail

# ATTENTION! Also update the corresponding version in CMakeLists.txt AND CMakePresets.json
CMAKE_VERSION=3.25.2
CMAKE_MIRROR="https://github.com/Kitware/CMake/releases/download/"
CMAKE_URL=$CMAKE_MIRROR/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
CMAKE_SHA256SUM="4d98de8d605da676e71a889dd94f80c76abb377fade2f21e3510e62ece1e1ada"


SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


source $SCRIPT_DIRECTORY/common.sh

process_cmdline_argument $@

TMP_DIR=$PKGROOT/var/tmp/cmake/

mkdir -p $TMP_DIR
mkdir -p $TMP_DIR/download

wget $CMAKE_URL -O $TMP_DIR/download/cmake.sh
pushd $TMP_DIR/download
if [[ $CMAKE_SHA256SUM != "SKIP" ]]; then
  echo "$CMAKE_SHA256SUM cmake.sh" | sha256sum --check
fi
bash cmake.sh --skip-license --prefix=$PKGROOT/usr
popd
