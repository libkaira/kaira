#!/bin/bash
# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------


process_cmdline_argument_help() {
  echo "Usage:"
  printf "  %-30s %-30s\n"  "dependency --help"  "Show This Help"
  printf "  %-30s %-30s\n"  "dependency <args>"  "Install Dependency"

  echo "Arguments:"
  printf "  %-40s %-30s\n"  "--toolchain <toolchain>" "Toolchain File"
  printf "  %-40s %-30s\n"  "--feature <feature>" "Project Features"
  printf "  %-40s %-30s\n"  "--platform <platform>" "Platform"
  printf "  %-40s %-30s\n"  "--pkgroot <pkgroot>" "Install pkgroot"
  printf "  %-40s %-30s\n"  "--std <std>" "Standard"

}

process_cmdline_argument() {

  TOOLCHAIN_SPECIFIED="false"
  FEATURE_SPECIFIED="false"
  PLATFORM_SPECIFIED="false"
  PKGROOT_SPECIFIED="false"
  STD_SPECIFIED="false"

  while [[ $# -gt 0 ]]; do
    local KEY=$1
    case $KEY in
      -h|--help)
        resolve_dep_help
        exit 0
        ;;
      --toolchain)
        shift
        if [[ $# -eq 0 ]]; then
          echo "Expected a value"
          break
        fi
        TOOLCHAIN=$1
        TOOLCHAIN_SPECIFIED="true"
        shift
        ;;
      --feature)
        shift
        if [[ $# -eq 0 ]]; then
          echo "Expected a value"
          break
        fi
        FEATURE=$1
        FEATURE_SPECIFIED="true"
        shift
        ;;
      --platform)
        shift
        if [[ $# -eq 0 ]]; then
          echo "Expected a value"
          break
        fi
        PLATFORM=$1
        PLATFORM_SPECIFIED="true"
        shift
        ;;
      --pkgroot)
        shift
        if [[ $# -eq 0 ]]; then
          echo "Expected a value"
          break
        fi
        PKGROOT=$1
        PKGROOT_SPECIFIED="true"
        shift
        ;;
      --std)
        shift
        if [[ $# -eq 0 ]]; then
          echo "Expected a value"
          break
        fi
        STD=$1
        STD_SPECIFIED="true"
        shift
        ;;
      *)
        echo "Unrecognized Option $KEY"
        process_cmdline_argument_help
        exit 1
    esac
  done

  local PRINT_HELP_AND_EXIT="false"

  if [[ $TOOLCHAIN_SPECIFIED == "false" ]]; then
    echo "Missing toolchain."
    local PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $FEATURE_SPECIFIED == "false" ]]; then
    echo "Missing feature."
    local PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $PLATFORM_SPECIFIED == "false" ]]; then
    echo "Missing platform."
    local PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $PKGROOT_SPECIFIED == "false" ]]; then
    echo "Missing pkgroot."
    local PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $STD_SPECIFIED == "false" ]]; then
    echo "Missing standard."
    local PRINT_HELP_AND_EXIT="true"
  fi

  echo ""

  if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
    process_cmdline_argument_help
    exit 1
  fi


}
