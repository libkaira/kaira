#!/bin/bash
# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
set -euo pipefail


TORA_URL="https://orgtora.gitlab.io/install.sh"


SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


source $SCRIPT_DIRECTORY/common.sh

process_cmdline_argument $@

export PATH=$PKGROOT/usr/bin:$PATH

TMP_DIR=$PKGROOT/var/tmp/tora/

mkdir -p $TMP_DIR/
pushd $TMP_DIR/
curl -L $TORA_URL -o install.sh
bash install.sh --prefix $PKGROOT/usr --tmp-dir $TMP_DIR/download
popd


