#!/bin/bash
# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

set -euo pipefail


JNUM_VERSION="main"
JNUM_MIRROR="https://gitlab.jsc.fz-juelich.de/libzell/software/jnum"
JNUM_URL="$JNUM_MIRROR/-/archive/$JNUM_VERSION/jnum-$JNUM_VERSION.tar.gz"
JNUM_SHA256SUM="SKIP"

SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


source $SCRIPT_DIRECTORY/common.sh

process_cmdline_argument $@

export PATH=$PKGROOT/usr/bin:$PATH

TMP_DIR=$PKGROOT/var/tmp/jnum/

mkdir -p $TMP_DIR
mkdir -p $TMP_DIR/download

wget $JNUM_URL -O $TMP_DIR/download/jnum.tar.gz

pushd $TMP_DIR/download
if [[ $JNUM_SHA256SUM != "SKIP" ]]; then
  echo "$JNUM_SHA256SUM jnum.tar.gz" | sha256sum --check
fi
popd


pushd $TMP_DIR
mkdir -p src
pushd src
mkdir -p jnum
tar xf ../download/jnum.tar.gz -C jnum --strip-components 1
popd

mkdir -p build
pushd build
cmake --toolchain $TOOLCHAIN --install-prefix $PKGROOT/usr ../src/jnum -DCMAKE_BUILD_TYPE=Release

cmake --build .
cmake --install .
popd
popd

