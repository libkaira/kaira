#!/bin/bash
# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

set -euo pipefail


CATCH2_VERSION="v3.3.2"
CATCH2_VERSION_TYPE="tags"
CATCH2_MIRROR="https://github.com/catchorg/Catch2"
CATCH2_URL="$CATCH2_MIRROR/archive/refs/$CATCH2_VERSION_TYPE/$CATCH2_VERSION.tar.gz"
CATCH2_SHA256SUM="8361907f4d9bff3ae7c1edb027f813659f793053c99b67837a0c0375f065bae2"

SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


source $SCRIPT_DIRECTORY/common.sh

process_cmdline_argument $@

export PATH=$PKGROOT/usr/bin:$PATH

TMP_DIR=$PKGROOT/var/tmp/catch2/

mkdir -p $TMP_DIR
mkdir -p $TMP_DIR/download

wget $CATCH2_URL -O $TMP_DIR/download/catch2.tar.gz

pushd $TMP_DIR/download
if [[ $CATCH2_SHA256SUM != "SKIP" ]]; then
  echo "$CATCH2_SHA256SUM catch2.tar.gz" | sha256sum --check
fi
popd


pushd $TMP_DIR
mkdir -p src
pushd src
mkdir -p catch2
tar xf ../download/catch2.tar.gz -C catch2 --strip-components 1
popd

mkdir -p build
pushd build
cmake --toolchain $TOOLCHAIN --install-prefix $PKGROOT/usr ../src/catch2 -DCMAKE_BUILD_TYPE=Release

cmake --build .
cmake --install .
popd
popd

