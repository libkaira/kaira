#!/bin/bash
# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
set -euo pipefail


NLOHMANN_VERSION="3.11.2"
NLOHMANN_MIRROR="https://github.com/nlohmann/json/archive/refs/tags/"
NLOHMANN_URL=$NLOHMANN_MIRROR/v$NLOHMANN_VERSION.tar.gz
NLOHMANN_SHA256SUM="d69f9deb6a75e2580465c6c4c5111b89c4dc2fa94e3a85fcd2ffcd9a143d9273"

SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


source $SCRIPT_DIRECTORY/common.sh

process_cmdline_argument $@

export PATH=$PKGROOT/usr/bin:$PATH

TMP_DIR=$PKGROOT/var/tmp/json/

mkdir -p $TMP_DIR
mkdir -p $TMP_DIR/download

wget $NLOHMANN_URL -O $TMP_DIR/download/json.tar.gz

pushd $TMP_DIR/download
if [[ $NLOHMANN_SHA256SUM != "SKIP" ]]; then
  echo "$NLOHMANN_SHA256SUM json.tar.gz" | sha256sum --check
fi
popd


pushd $TMP_DIR
mkdir -p src
pushd src
mkdir -p json
tar xf ../download/json.tar.gz -C json --strip-components 1
popd

mkdir -p build
pushd build
cmake --toolchain $TOOLCHAIN --install-prefix $PKGROOT/usr ../src/json -DJSON_BuildTests=Off
cmake --build .
cmake --install .
popd
popd

