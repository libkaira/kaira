# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

set -euo pipefail

if [[ $# -ne 1 ]]; then
  echo "You provided $# arguments."
  echo "The script accepts 1 argument of a ';'-separated feature string."
  exit 1
fi

source /opt/rh/gcc-toolset-12/enable
