# ------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# ------------------------------------------------------------------------------

export STDPROFILE_C_STD=17
export STDPROFILE_CXX_STD=23
export STDPROFILE_HIP_STD=23
export STDPROFILE_CUDA_STD=23
