#!/bin/bash
#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2020, Jayesh Badwaik <jayesh@badwaik.dev>
#-------------------------------------------------------------------------------
set -euo pipefail

SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
LIB_DIRECTORY=$SCRIPT_DIRECTORY/../lib/

resolve_dep_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "resolve.dep --help"  "Display this help"
  printf "  %-30s %-30s\n"  "resolve.dep <args>" "Dependency Resolver"

  echo "Arguments:"
  printf "  %-40s %-30s\n"  "[--toolchain <toolchain>]" "Toolchain File"
  printf "  %-40s %-30s\n"  "--feature <feature>" "Project Features"
  printf "  %-40s %-30s\n"  "--platform <platform>" "Platform"
  printf "  %-40s %-30s\n"  "--pkgroot <pkgroot>" "Install pkgroot"
  printf "  %-40s %-30s\n"  "--std <std>" "Standard"
}

if [[ $# -eq 0 ]]; then
  resolve_dep_help
  exit 1
fi

STD_SPECIFIED="false"
PLATFORM_SPECIFIED="false"
FEATURE_SPECIFIED="false"
PKGROOT_SPECIFIED="false"
TOOLCHAIN_SPECIFIED="false"

while [[ $# -gt 0 ]]; do
  KEY=$1
  case $KEY in
    -h|--help)
      resolve_dep_help
      exit 0
      ;;
    --toolchain)
      shift
      if [[ $# -eq 0 ]]; then
        echo "Expected a value"
        break
      fi
      TOOLCHAIN=$1
      TOOLCHAIN_SPECIFIED="true"
      shift
      ;;
    --feature)
      shift
      if [[ $# -eq 0 ]]; then
        echo "Expected a value"
        break
      fi
      FEATURE=$1
      FEATURE_SPECIFIED="true"
      shift
      ;;
    --platform)
      shift
      if [[ $# -eq 0 ]]; then
        echo "Expected a value"
        break
      fi
      PLATFORM=$1
      PLATFORM_SPECIFIED="true"
      shift
      ;;
    --pkgroot)
      shift
      if [[ $# -eq 0 ]]; then
        echo "Expected a value"
        break
      fi
      PKGROOT=$1
      PKGROOT_SPECIFIED="true"
      shift
      ;;
    --std)
      shift
      if [[ $# -eq 0 ]]; then
        echo "Expected a value"
        break
      fi
      STD=$1
      STD_SPECIFIED="true"
      shift
      ;;
    *)
      echo "Unrecognized Option $KEY"
      resolve_dep_help
      exit 1
  esac
done

PRINT_HELP_AND_EXIT="false"

if [[ $FEATURE_SPECIFIED == "false" ]]; then
  echo "Missing feature."
  PRINT_HELP_AND_EXIT="true"
fi

if [[ $PLATFORM_SPECIFIED == "false" ]]; then
  echo "Missing platform."
  PRINT_HELP_AND_EXIT="true"
fi

if [[ $PKGROOT_SPECIFIED == "false" ]]; then
  echo "Missing pkgroot."
  PRINT_HELP_AND_EXIT="true"
fi

if [[ $STD_SPECIFIED == "false" ]]; then
  echo "Missing standard."
  PRINT_HELP_AND_EXIT="true"
fi

echo ""

if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
  resolve_dep_help
  exit 1
fi

mkdir -p $PKGROOT
PKGROOT=$(realpath $PKGROOT)

if [[ $STD == "current" ]]; then
  source $LIB_DIRECTORY/stdprofile/current.sh
elif [[ $STD == "future" ]]; then
  source $LIB_DIRECTORY/stdprofile/future.sh
elif [[ $STD == "past" ]]; then
  source $LIB_DIRECTORY/stdprofile/past.sh
else
  echo "Unsupported value for --std."
  echo "Supported values are : current, future, past"
  exit 1
fi

#-------------------------------------------------------------------------------
# Install Dependencies
#-------------------------------------------------------------------------------

SITE_TOOLCHAIN_FILE=$PKGROOT/srv/toolchain/cmake/toolchain.cmake

$LIB_DIRECTORY/dep/jnum.sh \
  --toolchain $SITE_TOOLCHAIN_FILE \
  --feature $FEATURE \
  --platform $PLATFORM \
  --pkgroot $PKGROOT \
  --std $STD

$LIB_DIRECTORY/dep/expected.sh \
  --toolchain $SITE_TOOLCHAIN_FILE \
  --feature $FEATURE \
  --platform $PLATFORM \
  --pkgroot $PKGROOT \
  --std $STD


