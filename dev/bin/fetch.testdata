#!/bin/bash
#-------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2020, Jayesh Badwaik <jayesh@badwaik.dev>
#-------------------------------------------------------------------------------
set -euo pipefail

SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
ETC_DIRECTORY=$SCRIPT_DIRECTORY/../etc

fetch_testdata_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "resolve.dep --help"  "Display this help"
  printf "  %-30s %-30s\n"  "resolve.dep <args>" "Dependency Resolver"

  echo "Arguments:"
  printf "  %-40s %-30s\n"  "--dest <destination>" "Destination"
  printf "  %-40s %-30s\n"  "[--version <version>]" "Version"
}

if [[ $# -eq 0 ]]; then
  fetch_testdata_help
  exit 1
fi


DEST_SPECIFIED="false"

while [[ $# -gt 0 ]]; do
  KEY=$1
  case $KEY in
    -h|--help)
      fetch_testdata_help
      exit 0
      ;;
    --dest)
      shift
      if [[ $# -eq 0 ]]; then
        echo "Expected a value"
        break
      fi
      DEST=$1
      DEST_SPECIFIED="true"
      shift
      ;;
    *)
      echo "Unrecognized Option $KEY"
      fetch_testdata_help
      exit 1
  esac
done

PRINT_HELP_AND_EXIT="false"

if [[ $DEST_SPECIFIED == "false" ]]; then
  echo "Missing destination."
  PRINT_HELP_AND_EXIT="true"
fi

echo ""

if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
  fetch_testdata_help
  exit 1
fi

mkdir -p $DEST/testdata
pushd $DEST
  source $ETC_DIRECTORY/testdata.sh
  curl -fsSL $TESTDATA_URL | tar xzf - -C testdata --strip-components 1
popd

