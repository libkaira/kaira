# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------


TESTDATA_BASE_URL="https://gitlab.jsc.fz-juelich.de/libzell/data/testdata/-/archive"
export TESTDATA_URL="$TESTDATA_BASE_URL/main/testdata-main.tar.gz"
